import os
import lazylibrarian
from lazylibrarian import logger
from lazylibrarian.scheduling import notifyStrings, NOTIFY_SNATCH, NOTIFY_DOWNLOAD, NOTIFY_FAIL
from lazylibrarian.common import syspath

try:
    import gntp.notifier as gntp_notifier
except ImportError:
    import lib.gntp.notifier as gntp_notifier


class GrowlNotifier:
    def __init__(self):
        pass

    @staticmethod
    def _send_growl(growl_host=None, growl_password=None, event=None, message=None, force=False):

        title = "LazyLibrarian"

        # suppress notifications if the notifier is disabled but the notify options are checked
        if not lazylibrarian.CONFIG['USE_GROWL'] and not force:
            return False

        if not growl_host:
            growl_host = lazylibrarian.CONFIG['GROWL_HOST']

        if growl_password is None:
            growl_password = lazylibrarian.CONFIG['GROWL_PASSWORD']

        logger.debug(u"Growl: title: " + title)
        logger.debug(u"Growl: event: " + event)
        logger.debug(u"Growl: message: " + message)

        # Split host and port
        try:
            host, port = growl_host.split(':', 1)
            port = int(port)
        except ValueError:
            logger.debug("Invalid growl host, using default")
            host, port = 'localhost', 23053

        # If password is empty, assume none
        if not growl_password:
            growl_password = None

        try:
            # Register notification
            growl = gntp_notifier.GrowlNotifier(
                applicationName='LazyLibrarian',
                notifications=['New Event'],
                defaultNotifications=['New Event'],
                hostname=host,
                port=port,
                password=growl_password
            )
        except Exception as e:
            logger.error(e)
            return False

        try:
            growl.register()
        except gntp_notifier.errors.NetworkError:
            logger.warn(u'Growl notification failed: network error')
            return False

        except gntp_notifier.errors.AuthError:
            logger.warn(u'Growl notification failed: authentication error')
            return False

        # Send it, including an image if available
        image_file = os.path.join(lazylibrarian.PROG_DIR, "data/images/ll.png")
        if os.path.exists(image_file):
            with open(syspath(image_file), 'rb') as f:
                image = f.read()
        else:
            image = None

        try:
            # noinspection PyTypeChecker
            growl.notify(
                noteType='New Event',
                title=event,
                description=message,
                icon=image
            )
        except gntp_notifier.errors.NetworkError:
            logger.warn(u'Growl notification failed: network error')
            return False

        logger.info(u"Growl notification sent.")
        return True

    #
    # Public functions
    #

    def notify_snatch(self, title, fail=False):
        if lazylibrarian.CONFIG['GROWL_ONSNATCH']:
            if fail:
                self._send_growl(growl_host='', growl_password=None, event=notifyStrings[NOTIFY_FAIL], message=title)
            else:
                self._send_growl(growl_host='', growl_password=None, event=notifyStrings[NOTIFY_SNATCH], message=title)

    def notify_download(self, title):
        if lazylibrarian.CONFIG['GROWL_ONDOWNLOAD']:
            self._send_growl(growl_host='', growl_password=None, event=notifyStrings[NOTIFY_DOWNLOAD], message=title)

    # noinspection PyUnusedLocal
    def test_notify(self, title="Test"):
        return self._send_growl(growl_host='', growl_password=None, event="Test",
                                message="Testing Growl settings from LazyLibrarian", force=True)


notifier = GrowlNotifier
